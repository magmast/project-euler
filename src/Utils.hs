module Utils where

import Data.List (tails)

adjacents :: Int -> [a] -> [[a]]
adjacents n xs = zipWith const (take n <$> tails xs) (drop (n - 1) xs)

isInteger :: (Num a, RealFrac a) => a -> Bool
isInteger x = fromInteger (round x) == x
