module Prime (factors, isPrime, primes) where

import Control.Monad (guard)
import Data.Foldable (find)

isPrime :: Int -> Bool
isPrime x
  | x == 2 = True
  | x > 2 = all (\y -> x `mod` y /= 0) [2 .. (ceiling $ sqrt $ fromIntegral x)]
  | otherwise = False

primes :: [Int]
primes = do
  x <- [2 ..]
  guard $ isPrime x
  pure x

factors :: Int -> [Int]
factors x = case find ((== 0) . (x `mod`)) primes of
  (Just factor) ->
    let next = x `div` factor
     in if isPrime next
          then [factor, next]
          else factor : factors next
  Nothing -> []