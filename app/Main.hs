module Main where

import Euler (answer15)

main :: IO ()
main = print answer15
